package com.anto.kycapp.Models;

public class ValidateOtpRequestTo {
    String entityId,otp,hashPrimary;

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getHashPrimary() {
        return hashPrimary;
    }

    public void setHashPrimary(String hashPrimary) {
        this.hashPrimary = hashPrimary;
    }
}
