package com.anto.kycapp.Models.PidBlockModels;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;
import org.simpleframework.xml.Text;

@Root(name = "Data")
public class Data {

    public Data() {
    }

    @Attribute(name = "type", required = false)
    public String type;

    @Text
    public String value;
}
