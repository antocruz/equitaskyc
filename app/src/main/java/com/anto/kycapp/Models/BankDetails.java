package com.anto.kycapp.Models;

import java.util.List;

public class BankDetails {
    String description,bin;
    List<BusinessDetails> businessDetails;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    public List<BusinessDetails> getBusinessDetails() {
        return businessDetails;
    }

    public void setBusinessDetails(List<BusinessDetails> businessDetails) {
        this.businessDetails = businessDetails;
    }
}
