package com.anto.kycapp.Models;

public class AllRequestTo {

    private String encryptedKey;
    private String encryptedPayload;

    public String getEncryptedKey() {
        return encryptedKey;
    }

    public void setEncryptedKey(String encryptedKey) {
        this.encryptedKey = encryptedKey;
    }

    public String getEncryptedpayload() {
        return encryptedPayload;
    }

    public void setEncryptedpayload(String encryptedpayload) {
        this.encryptedPayload = encryptedpayload;
    }
}
