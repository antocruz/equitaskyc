package com.anto.kycapp.Models;

public class GenOtpAndValidateUserRequestTo {
    public String username,entityId;

    public String getUserName() {
        return username;
    }

    public void setUserName(String userName) {
        this.username = userName;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }
}
