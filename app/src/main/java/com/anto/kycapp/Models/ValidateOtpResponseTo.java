package com.anto.kycapp.Models;

public class ValidateOtpResponseTo extends ResponseTO{
    Boolean result;
    String hashPrimary;

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getHashPrimary() {
        return hashPrimary;
    }

    public void setHashPrimary(String hashPrimary) {
        this.hashPrimary = hashPrimary;
    }
}
