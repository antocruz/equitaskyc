package com.anto.kycapp.Models.PidBlockModels;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name = "Uses")
public class Uses {

    public Uses() {
    }

    @Attribute(name = "pi", required = false)
    public String pi;

    @Attribute(name = "pa", required = false)
    public String pa;

    @Attribute(name = "pfa", required = false)
    public String pfa;

    @Attribute(name = "iType", required = false)
    public String iType;

    @Attribute(name = "pin", required = false)
    public String pin;

    @Attribute(name = "bio", required = false)
    public String bio;

    @Attribute(name = "bt", required = false)
    public String bt;

    @Attribute(name = "otp", required = false)
    public String otp;

}
