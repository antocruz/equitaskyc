package com.anto.kycapp.Models;

public class ValidationRequestTo {
    private String idType,idNumber,entityId,idTypeValidation,otpReferenceNo,otp,stan,consent,pidBlock;
    private String otpVerificationCode;
    private String agentName = "John";
    private String agentUcic= "123456";
    private String agentAccountNumber= "789456123";
    private String agentAddress= "chennai";
    private String agentCity= "chennai";
    private String agentState= "TN";
    private String agentPincode= "625001";
    private String corporateName= "Equitas";
    private String corporateAddress= "chennai";
    private String corporateCity= "chennai";
    private String corporateState= "TN";
    private String corporatePincode= "325001";
    private String fundSource= "Salary";
    private String signature;
    private String skey;
    private String ci;
    private String dataType;
    private String dataValue;
    private String hmac;

    public String getSkey() {
        return skey;
    }

    public void setSkey(String skey) {
        this.skey = skey;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDataValue() {
        return dataValue;
    }

    public void setDataValue(String dataValue) {
        this.dataValue = dataValue;
    }

    public String getHmac() {
        return hmac;
    }

    public void setHmac(String hmac) {
        this.hmac = hmac;
    }

    public String getOtpVerificationCode() {
        return otpVerificationCode;
    }

    public void setOtpVerificationCode(String otpVerificationCode) {
        this.otpVerificationCode = otpVerificationCode;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentUcic() {
        return agentUcic;
    }

    public void setAgentUcic(String agentUcic) {
        this.agentUcic = agentUcic;
    }

    public String getAgentAccountNumber() {
        return agentAccountNumber;
    }

    public void setAgentAccountNumber(String agentAccountNumber) {
        this.agentAccountNumber = agentAccountNumber;
    }

    public String getAgentAddress() {
        return agentAddress;
    }

    public void setAgentAddress(String agentAddress) {
        this.agentAddress = agentAddress;
    }

    public String getAgentCity() {
        return agentCity;
    }

    public void setAgentCity(String agentCity) {
        this.agentCity = agentCity;
    }

    public String getAgentState() {
        return agentState;
    }

    public void setAgentState(String agentState) {
        this.agentState = agentState;
    }

    public String getAgentPincode() {
        return agentPincode;
    }

    public void setAgentPincode(String agentPincode) {
        this.agentPincode = agentPincode;
    }

    public String getCorporateName() {
        return corporateName;
    }

    public void setCorporateName(String corporateName) {
        this.corporateName = corporateName;
    }

    public String getCorporateAddress() {
        return corporateAddress;
    }

    public void setCorporateAddress(String corporateAddress) {
        this.corporateAddress = corporateAddress;
    }

    public String getCorporateCity() {
        return corporateCity;
    }

    public void setCorporateCity(String corporateCity) {
        this.corporateCity = corporateCity;
    }

    public String getCorporateState() {
        return corporateState;
    }

    public void setCorporateState(String corporateState) {
        this.corporateState = corporateState;
    }

    public String getCorporatePincode() {
        return corporatePincode;
    }

    public void setCorporatePincode(String corporatePincode) {
        this.corporatePincode = corporatePincode;
    }

    public String getFundSource() {
        return fundSource;
    }

    public void setFundSource(String fundSource) {
        this.fundSource = fundSource;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getConsent() {
        return consent;
    }

    public void setConsent(String consent) {
        this.consent = consent;
    }

    public String getPidBlock() {
        return pidBlock;
    }

    public void setPidBlock(String pidBlock) {
        this.pidBlock = pidBlock;
    }

    public String getStan() {
        return stan;
    }

    public void setStan(String stan) {
        this.stan = stan;
    }

    public String getIdType() {
        return idType;
    }

    public void setIdType(String idType) {
        this.idType = idType;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getIdTypeValidation() {
        return idTypeValidation;
    }

    public void setIdTypeValidation(String idTypeValidation) {
        this.idTypeValidation = idTypeValidation;
    }

    public String getOtpReferenceNo() {
        return otpReferenceNo;
    }

    public void setOtpReferenceNo(String otpReferenceNo) {
        this.otpReferenceNo = otpReferenceNo;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
