package com.anto.kycapp.Models;

import java.util.List;

/**
 * Created by admin on 31-10-2015.
 */
public class AgentLoginResponseTO extends ResponseTO {
    Result result;


    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {
        boolean success;
        String firstName, lastName, userType,authType,userName,token;
        private List<ApplicationDetail> applicationDetails = null;
        private List<BusinessDetails> businessDetails = null;
        private List<BankDetails> bankDetails = null;

        LoginDetail loginDetail;

        public LoginDetail getLoginDetail() {
            return loginDetail;
        }

        public void setLoginDetail(LoginDetail loginDetail) {
            this.loginDetail = loginDetail;
        }


        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean isSuccess) {
            this.success = isSuccess;
        }

        public boolean getSuccess() {
            return success;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUserType() {
            return userType;
        }

        public void setUserType(String userType) {
            this.userType = userType;
        }

        public String getAuthType() {
            return authType;
        }

        public void setAuthType(String authType) {
            this.authType = authType;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getToken() {
            return token;
        }

        public List<ApplicationDetail> getApplicationDetails() {
            return applicationDetails;
        }

        public void setApplicationDetails(List<ApplicationDetail> applicationDetails) {
            this.applicationDetails = applicationDetails;
        }

        public List<BusinessDetails> getBusinessDetails() {
            return businessDetails;
        }

        public void setBusinessDetails(List<BusinessDetails> businessDetails) {
            this.businessDetails = businessDetails;
        }

        public List<BankDetails> getBankDetails() {
            return bankDetails;
        }

        public void setBankDetails(List<BankDetails> bankDetails) {
            this.bankDetails = bankDetails;
        }
    }
}
