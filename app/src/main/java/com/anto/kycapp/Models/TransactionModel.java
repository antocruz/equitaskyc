package com.anto.kycapp.Models;

import android.os.Parcel;
import android.os.Parcelable;

public class TransactionModel implements Parcelable{
    private String txRef;
    private String beneficiaryName;
    private String beneficiaryId,beneficiaryType,txnOrigin,status;
    private String otherPartyName;
    private String yourWallet;
    private String beneficiaryWallet;
    public String description,retrivalReferenceNo,authCode;
    private String businessId;
    private String bankTid,externalTransactionId;


    public TransactionModel(){}
    protected TransactionModel(Parcel in) {
        txRef = in.readString();
        beneficiaryName = in.readString();
        beneficiaryId = in.readString();
        otherPartyName = in.readString();
        yourWallet = in.readString();
        beneficiaryWallet = in.readString();
        description = in.readString();
        txnOrigin = in.readString();
        status = in.readString();
        beneficiaryType = in.readString();
        retrivalReferenceNo = in.readString();
        authCode = in.readString();
        rowId = in.readLong();
        time = in.readLong();
        amount = in.readFloat();
        balance = in.readFloat();
        isSplited = in.readByte() != 0;
        transactionId = in.readString();
        transactionType = in.readString();
        merchantName = in.readString();
        merchantId = in.readString();
        transactionDate = in.readString();
        transactionStatus = in.readString();
        transactionMode = in.readString();
        type = in.readString();
        transactionDateTime = in.readString();
    }

    public static final Creator<TransactionModel> CREATOR = new Creator<TransactionModel>() {
        @Override
        public TransactionModel createFromParcel(Parcel in) {
            return new TransactionModel(in);
        }

        @Override
        public TransactionModel[] newArray(int size) {
            return new TransactionModel[size];
        }
    };

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTransactionMode() {
        return transactionMode;
    }

    public void setTransactionMode(String transactionMode) {
        this.transactionMode = transactionMode;
    }

    public String getBeneficiaryType() {
        return beneficiaryType;
    }

    public void setBeneficiaryType(String beneficiaryType) {
        this.beneficiaryType = beneficiaryType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTxnOrigin() {
        return txnOrigin;
    }

    public void setTxnOrigin(String txnOrigin) {
        this.txnOrigin = txnOrigin;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public long getRowId() {
        return rowId;
    }

    public void setRowId(long rowId) {
        this.rowId = rowId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getMerchantName() {
        if (merchantName == null) {
            return "";
        } else {
            return merchantName;
        }
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    long rowId,time;
    float amount, balance;
    boolean isSplited;
    String transactionId, transactionType, merchantName, merchantId, transactionDate, transactionStatus, transactionMode, type, transactionDateTime;


    public boolean getIsSplited() {
        return isSplited;
    }

    public void setIsSplited(boolean isSplited) {
        this.isSplited = isSplited;
    }

    public String getTxRef() {
        return txRef;
    }

    public void setTxRef(String txRef) {
        this.txRef = txRef;
    }

    public String getBeneficiaryName() {
        return beneficiaryName;
    }

    public void setBeneficiaryName(String beneficiaryName) {
        this.beneficiaryName = beneficiaryName;
    }

    public String getTransactionDateTime() {
        return transactionDateTime;
    }

    public void setTransactionDateTime(String transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }

    public String getOtherPartyName() {
        return otherPartyName;
    }

    public void setOtherPartyName(String otherPartyName) {
        this.otherPartyName = otherPartyName;
    }

    public String getYourWallet() {
        return yourWallet;
    }

    public void setYourWallet(String yourWallet) {
        this.yourWallet = yourWallet;
    }

    public String getBeneficiaryWallet() {
        return beneficiaryWallet;
    }

    public void setBeneficiaryWallet(String beneficiaryWallet) {
        this.beneficiaryWallet = beneficiaryWallet;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isSplited() {
        return isSplited;
    }

    public void setSplited(boolean splited) {
        isSplited = splited;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getBeneficiaryId() {
        return beneficiaryId;
    }

    public void setBeneficiaryId(String beneficiaryId) {
        this.beneficiaryId = beneficiaryId;
    }

    public String getRetrivalReferenceNo() {
        return retrivalReferenceNo;
    }

    public void setRetrivalReferenceNo(String retrivalReferenceNo) {
        this.retrivalReferenceNo = retrivalReferenceNo;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(txRef);
        dest.writeString(beneficiaryName);
        dest.writeString(beneficiaryId);
        dest.writeString(otherPartyName);
        dest.writeString(yourWallet);
        dest.writeString(beneficiaryWallet);
        dest.writeString(description);
        dest.writeString(retrivalReferenceNo);
        dest.writeString(authCode);
        dest.writeLong(rowId);
        dest.writeLong(time);
        dest.writeFloat(amount);
        dest.writeFloat(balance);
        dest.writeByte((byte) (isSplited ? 1 : 0));
        dest.writeString(transactionId);
        dest.writeString(transactionType);
        dest.writeString(merchantName);
        dest.writeString(merchantId);
        dest.writeString(transactionDate);
        dest.writeString(transactionStatus);
        dest.writeString(transactionMode);
        dest.writeString(type);
        dest.writeString(transactionDateTime);
    }

    public String getBankTid() {
        return bankTid;
    }

    public void setBankTid(String bankTid) {
        this.bankTid = bankTid;
    }

    public String getExternalTransactionId() {
        return externalTransactionId;
    }

    public void setExternalTransactionId(String externalTransactionId) {
        this.externalTransactionId = externalTransactionId;
    }
}