package com.anto.kycapp.Models.PidBlockModels;


import android.util.Base64;

public class PidblockModels {

    private String entityId;
    private String consent;
    private String idTypeValidation;
    private String idNumber;
    private String skey;
    private String ci;
    private String mc;
    private String dataType;
    private String dataValue;
    private String hmac;
    private String mi;
    private String rdsId;
    private String rdsVer;
    private String dpId;
    private String dc;
    private String agentName;
    private String agentUcic;
    private String agentAccountNumber;
    private String agentAddress;
    private String agentCity;
    private String agentState;
    private String agentPincode;
    private String corporateName;
    private String corporateAddress;
    private String corporateCity;
    private String corporateState;
    private String corporatePincode;
    private String fundSource;
    private String signature;

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentUcic() {
        return agentUcic;
    }

    public void setAgentUcic(String agentUcic) {
        this.agentUcic = agentUcic;
    }

    public String getAgentAccountNumber() {
        return agentAccountNumber;
    }

    public void setAgentAccountNumber(String agentAccountNumber) {
        this.agentAccountNumber = agentAccountNumber;
    }

    public String getAgentAddress() {
        return agentAddress;
    }

    public void setAgentAddress(String agentAddress) {
        this.agentAddress = agentAddress;
    }

    public String getAgentCity() {
        return agentCity;
    }

    public void setAgentCity(String agentCity) {
        this.agentCity = agentCity;
    }

    public String getAgentState() {
        return agentState;
    }

    public void setAgentState(String agentState) {
        this.agentState = agentState;
    }

    public String getAgentPincode() {
        return agentPincode;
    }

    public void setAgentPincode(String agentPincode) {
        this.agentPincode = agentPincode;
    }

    public String getCorporateName() {
        return corporateName;
    }

    public void setCorporateName(String corporateName) {
        this.corporateName = corporateName;
    }

    public String getCorporateAddress() {
        return corporateAddress;
    }

    public void setCorporateAddress(String corporateAddress) {
        this.corporateAddress = corporateAddress;
    }

    public String getCorporateCity() {
        return corporateCity;
    }

    public void setCorporateCity(String corporateCity) {
        this.corporateCity = corporateCity;
    }

    public String getCorporateState() {
        return corporateState;
    }

    public void setCorporateState(String corporateState) {
        this.corporateState = corporateState;
    }

    public String getCorporatePincode() {
        return corporatePincode;
    }

    public void setCorporatePincode(String corporatePincode) {
        this.corporatePincode = corporatePincode;
    }

    public String getFundSource() {
        return fundSource;
    }

    public void setFundSource(String fundSource) {
        this.fundSource = fundSource;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public  Wadh wadh = new Wadh();

    public Wadh getWadh() {
        return wadh;
    }

    public void setWadh(Wadh wadh) {
        this.wadh = wadh;
    }

    public class Wadh{
        String ts,ver,ra,rc,lr,de,pfr;

        public String getTs() {
            return ts;
        }

        public void setTs(String ts) {
            this.ts = ts;
        }

        public String getVer() {
            return ver;
        }

        public void setVer(String ver) {
            this.ver = ver;
        }

        public String getRa() {
            return ra;
        }

        public void setRa(String ra) {
            this.ra = ra;
        }

        public String getRc() {
            return rc;
        }

        public void setRc(String rc) {
            this.rc = rc;
        }

        public String getLr() {
            return lr;
        }

        public void setLr(String lr) {
            this.lr = lr;
        }

        public String getDe() {
            return de;
        }

        public void setDe(String de) {
            this.de = de;
        }

        public String getPfr() {
            return pfr;
        }

        public void setPfr(String pfr) {
            this.pfr = pfr;
        }
    }


    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getConsent() {
        return consent;
    }

    public void setConsent(String consent) {
        this.consent = consent;
    }

    public String getIdTypeValidation() {
        return idTypeValidation;
    }

    public void setIdTypeValidation(String idTypeValidation) {
        this.idTypeValidation = idTypeValidation;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getSkey() {
        return skey;
    }

    public void setSkey(String skey) {
        this.skey = skey;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

    public String getMc() {
        return mc;
    }

    public void setMc(String mc) {
        this.mc = mc;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getDataValue() {
        return dataValue;
    }

    public void setDataValue(String dataValue) {
        this.dataValue = dataValue;
    }

    public String getHmac() {
        return hmac;
    }

    public void setHmac(String hmac) {
        this.hmac = hmac;
    }

    public String getMi() {
        return mi;
    }

    public void setMi(String mi) {
        this.mi = mi;
    }

    public String getRdsId() {
        return rdsId;
    }

    public void setRdsId(String rdsId) {
        this.rdsId = rdsId;
    }

    public String getRdsVer() {
        return rdsVer;
    }

    public void setRdsVer(String rdsVer) {
        this.rdsVer = rdsVer;
    }

    public String getDpId() {
        return dpId;
    }

    public void setDpId(String dpId) {
        this.dpId = dpId;
    }

    public String getDc() {
        return dc;
    }

    public void setDc(String dc) {
        this.dc = dc;
    }



}