package com.anto.kycapp.Models;

/**
 * Created by anto on 2/18/2019.
 */

public class Data {

    Role role;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public class Role {
          String roleLevel,description,name;

        public String getRoleLevel() {
            return roleLevel;
        }

        public void setRoleLevel(String roleLevel) {
            this.roleLevel = roleLevel;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
