package com.anto.kycapp;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnPageErrorListener;


public class PdfViewActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pdfview_layout);


        PDFView pdfView = findViewById(R.id.activity_main_pdf_view);
        pdfView.fromAsset("aadhaarmultilingual.pdf").password(null).defaultPage(0).defaultPage(0).onPageError(new OnPageErrorListener() {
            @Override
            public void onPageError(int page, Throwable t) {
                Toast.makeText(
                        PdfViewActivity.this,
                "Error at page: $page", Toast.LENGTH_LONG
            ).show();
            }
        }).load();


    }
}
