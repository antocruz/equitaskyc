package com.anto.kycapp.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.anto.kycapp.Fragments.UploadKycFragment;
import com.anto.kycapp.Models.PendingkycResponse;
import com.anto.kycapp.Models.PendingsignupResponse;
import com.anto.kycapp.R;
import com.anto.kycapp.Utils.FontChangeCrawler;
import com.anto.kycapp.Utils.MyBounceInterpolator;


import java.util.ArrayList;

/**
 * Created by anto on 2/14/2018.
 */

public class CustomBaseKycAdapter extends BaseAdapter {
    ArrayList<PendingkycResponse> myList = new ArrayList();
    LayoutInflater inflater;
    FragmentActivity context;

    public CustomBaseKycAdapter(FragmentActivity context, ArrayList<PendingkycResponse> myList) {
        this.myList = myList;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }


    @Override
    public int getCount() {
        return myList.size();
    }

    @Override
    public PendingkycResponse getItem(int position) {
        return myList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final MyViewHolder mViewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.layout_list_item, parent, false);

            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        FontChangeCrawler fontChanger = new FontChangeCrawler(context.getAssets(), context.getResources().getString(R.string.appfontBold));
        fontChanger.replaceFonts((ViewGroup)convertView.findViewById(R.id.listitemlay));


        mViewHolder.corporate_txt.setText(myList.get(position).getBusiness());
        mViewHolder.customerid_txt.setText(myList.get(position).getEntityId());
        mViewHolder.dateandtime_txt.setText(myList.get(position).getCreated());
        if (myList.get(position).getKycStatus().equals("UPLOAD_KYC")) {
            mViewHolder.statuslay.setVisibility(View.GONE);
            mViewHolder.textView2.setVisibility(View.VISIBLE);
        } else {
            mViewHolder.statuslay.setVisibility(View.VISIBLE);
            mViewHolder.textView2.setVisibility(View.GONE);
            mViewHolder.status_txt.setText(myList.get(position).getKycStatus());
        }
        final Animation myAnim = AnimationUtils.loadAnimation(context, R.anim.bounce);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.1, 20);
        myAnim.setInterpolator(interpolator);
        myAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (myList.get(position).getKycStatus().equals("PENDING")) {
                    context.getSupportFragmentManager().beginTransaction()
                            .addToBackStack("mainactivity")
                            .add(R.id.pendingsignuplay, new UploadKycFragment(myList.get(position).getKycStatus(),myList.get(position).getEntityId(), myList.get(position).getId(), myList.get(position).getBusiness(),myList.get(position).geteKycRefNo(),myList.get(position).getDocumentType())).commit();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewHolder.content_baselay.startAnimation(myAnim);

                }
        });

        //Toast.makeText(context, myList.get(position).getStatus(), Toast.LENGTH_SHORT).show();
        return convertView;
    }

    private class MyViewHolder {
        TextView corporate_txt, customerid_txt,dateandtime_txt,status_txt,textView2,corporatetitle_txt;
        RelativeLayout content_baselay;
        ImageView img_trans;
        LinearLayout statuslay;

        public MyViewHolder(View item) {
            content_baselay = item.findViewById(R.id.listitemlay);
            corporatetitle_txt = item.findViewById(R.id.corporatetitle_txt);
            corporate_txt = item.findViewById(R.id.corporate_txt);
            customerid_txt = item.findViewById(R.id.customerid_txt);
            dateandtime_txt = item.findViewById(R.id.dateandtime_txt);
            status_txt = item.findViewById(R.id.status_txt);
            statuslay = item.findViewById(R.id.statuslay);
            textView2 = item.findViewById(R.id.textView2);
        }
    }


}
