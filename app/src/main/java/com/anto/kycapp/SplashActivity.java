package com.anto.kycapp;

import android.animation.Animator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewPropertyAnimator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.anto.kycapp.Models.AllRequestTo;
import com.anto.kycapp.Utils.Constants;
import com.anto.kycapp.Utils.DeviceUtils;
import retrofit.Callback;
import retrofit.RetrofitError;

public class SplashActivity extends Activity {
    ImageView swingingLogo;
    RelativeLayout logo_group;
    String logintype;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        swingingLogo = (ImageView) findViewById(R.id.swinging_logo);
        logo_group = (RelativeLayout) findViewById(R.id.logo_group);
        logintype= Constants.getSharedPref(this,getString(R.string.tokenkey),"logintype");
        Log.i("logintype",logintype);
        performAnimations();
    }

    private void performAnimations() {
        ViewPropertyAnimator swingAnimation = logo_group.animate();
        swingAnimation.setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                validateBusiness();
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }
        });
        //swingAnimation.setDuration(1000).translationYBy(800);
        swingAnimation.setDuration(1000).translationYBy(800);

    }

    private void validateBusiness(){
        if (Constants.isNetworkAvailable(this)) {
            try {
                Constants.getEkycServicesAPI(this,"yes").validateBusiness( new Callback<AllRequestTo>() {
                    @Override
                    public void success(AllRequestTo allResponseTo, retrofit.client.Response response) {
                        Constants.toastMessage(SplashActivity.this,"Success");
                        try {

                            Constants.applyTokenSharedPreference(SplashActivity.this,Constants.SESSION_ENCPRIMARY,allResponseTo.getEncryptedpayload());

                            if(new DeviceUtils().isDeviceRooted(getApplicationContext())){
                                showAlertDialogAndExitApp(getString(R.string.deviceroot_txt));
                            }
                            else{
                                if(logintype == null)
                                    logintype = "firsttime";


                                if (logintype.equals("loggedin")) {
                                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                                else{
                                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }

                            }

                        } catch (Exception e) {
                            Log.e("Exception++",Log.getStackTraceString(e));                        }
                    }

                    @Override
                    public void failure(RetrofitError retrofitError) {
                        Constants.toastMessage(SplashActivity.this,"Failure");
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }
        }else{
            Constants.showCustomDialog(this,"Check your internet connection");
            //Toast.makeText(getActivity(), "Check your internet connection", Toast.LENGTH_SHORT).show();
        }


    }

    public void showAlertDialogAndExitApp(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(SplashActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }
                });

        alertDialog.show();
    }

}
