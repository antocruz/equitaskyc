package com.anto.kycapp.Fragments;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.anto.kycapp.R;
import com.anto.kycapp.Utils.Constants;
import com.williamww.silkysignature.views.SignaturePad;

import java.io.ByteArrayOutputStream;

public class SignaturePadDialogFragment extends DialogFragment {

    SignaturePad signaturePad;
    TextView textView;
    String signaturestr;
    RelativeLayout backlay;
    ImageView imageview;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.signaturedialog_fragment,container,false);
        signaturePad = view.findViewById(R.id.signature_pad_kyc);
        textView = view.findViewById(R.id.clear);
        backlay = view.findViewById(R.id.back_img);
        imageview =  view.findViewById(R.id.imageView7);

        backlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!signaturePad.isEmpty()) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    signaturePad.getSignatureBitmap().compress(Bitmap.CompressFormat.JPEG, 70, baos); // bm is the bitmap object
                    byte[] b = baos.toByteArray();
                    signaturestr = Base64.encodeToString(b, Base64.NO_WRAP);
                    Constants.signaturestr = signaturestr;
                    Log.d("Signaturepad", signaturestr);

                   /* byte[] decodedString = Base64.decode(signaturestr, Base64.DEFAULT);
                    Bitmap base64Bitmap = BitmapFactory.decodeByteArray(decodedString, 0,
                            decodedString.length);
                    imageview.setImageBitmap(base64Bitmap);*/
                    dismiss();
                } else {
                    Toast.makeText(getContext(), "Enter signature", Toast.LENGTH_SHORT).show();
                }
            }
        });

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signaturePad.clear();
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        boolean setFullScreen = true;
        if (getArguments() != null) {
            setFullScreen = getArguments().getBoolean("fullScreen");
        }

        if (setFullScreen)
            setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);

        this.setCancelable(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public interface DialogListener {
        void onFinishEditDialog(String inputText);
    }


}