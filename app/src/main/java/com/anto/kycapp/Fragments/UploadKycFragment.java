package com.anto.kycapp.Fragments;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.anto.kycapp.BuildConfig;

import com.anto.kycapp.Interface.GetUsersServiceAPI;
import com.anto.kycapp.MainActivity;
import com.anto.kycapp.Models.AddkycAgentResponseTO;
import com.anto.kycapp.Models.AgentLoginResponseTO;
import com.anto.kycapp.Models.AllRequestTo;
import com.anto.kycapp.Models.CreateorEditKycRequestTo;
import com.anto.kycapp.Models.EditKycResponseTO;
import com.anto.kycapp.Models.GenOtpAndValidateUserRequestTo;
import com.anto.kycapp.Models.GenerateOtpResponseTo;
import com.anto.kycapp.Models.GetKycDetailResponseTO;
import com.anto.kycapp.Models.GetKycRequestTo;
import com.anto.kycapp.Models.OtpkycValidationResponseTo;
import com.anto.kycapp.Models.PidBlockModels.PidblockModels;
import com.anto.kycapp.Models.ResponseTO;
import com.anto.kycapp.Models.UpdateEntityRequestTo;
import com.anto.kycapp.Models.UserdataInputTo;
import com.anto.kycapp.Models.ValidateBioResponseTo;
import com.anto.kycapp.Models.ValidateOtpRequestTo;
import com.anto.kycapp.Models.ValidateOtpResponseTo;
import com.anto.kycapp.Models.ValidationRequestTo;
import com.anto.kycapp.PdfViewActivity;
import com.anto.kycapp.R;
import com.anto.kycapp.Utils.Constants;
import com.anto.kycapp.Utils.DeviceConnectivity;
import com.anto.kycapp.Utils.DeviceUtils;
import com.anto.kycapp.Utils.FontChangeCrawler;

import com.anto.kycapp.Utils.PidDataConfig;
import com.anto.kycapp.Utils.RequestResponseUtils;
import com.google.gson.Gson;
import com.tuyenmonkey.mkloader.MKLoader;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;


import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class UploadKycFragment extends Fragment {

    private static final int ANNUAL_INCOME = 250000;
    LinearLayout signupprooflay1,signupprooflay2,signupprooflay3,signupprooflay4,signupprooflay5,videokyc_lay,biokyc_lay,otpkyc_lay;
    CardView customeruploadcard,prooflinearlay2,prooflinearlay3,prooflinearlay4,prooflinearlay5;
    ImageView  imageView2, imageView3, imageView4, imageView5, imageView6,back_img;
    TextView text_proof1,text_proof2,text_proof3,text_proof4,text_proof5,reasondetailtxt;
    TextView uploadkyc1,uploadkyc2,uploadkyc3,uploadkyc4,uploadkyc5;
    Uri picUri,picUri1,picUri2,picUri3,picUri4;
    List<MultipartBody.Part> parts = new ArrayList<>();
    String imageFilePath, imageFileName;
    protected static final int mantraRequestCode = 5;
    String photoFileName = "kycdocument", proofphoto = "";
    protected static final int REQUEST_CAMERA = 0;
    protected static final int REQUEST_VIDEO = 1;
    CardView reasoncard,gotoekyc;
    int documentslength;
    boolean clickfirst = true;
    TextView adduser_txt;
    EditText et_aadhaar,et_otp,fathername,mothermaiden,grossannual;
    String stan, xml="",otpVerificationCode;
    TextView submitotpkyc_txt;
    MKLoader mkloader;
    Activity activity;
    Dialog dialog;
    RadioGroup isagri,genderradio,martialstatus;
    String signaturestr,gender,martialStatus;
    Button submit_update;
    String customerid,id,business,status,documentType,kitno,primaryHash;
    Boolean isAgri;
    UserdataInputTo userdataInputTo;
    CreateorEditKycRequestTo createorEditKycRequestTo = new CreateorEditKycRequestTo();
    public UploadKycFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public UploadKycFragment(String status,String customerid, String id, String business,String kitno,String documentType){
        this.status = status;
        this.customerid = customerid;
        this.id = id;
        this.business = business;
        this.kitno= kitno;
        this.documentType= documentType;
    }

    Spinner proofSpinner1,proofSpinner2,proofSpinner3,proofSpinner4,proofSpinner5;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upload_kyc, container, false);
        activity = getActivity();
        FontChangeCrawler fontChanger = new FontChangeCrawler(activity.getAssets(), activity.getResources().getString(R.string.appfont));
        fontChanger.replaceFonts((ViewGroup)view.findViewById(R.id.uploadkycdoclay));


        reasoncard = (CardView)view.findViewById(R.id.reasoncard);
        gotoekyc = (CardView)view.findViewById(R.id.gotoekyc);


        signupprooflay1 = (LinearLayout)view.findViewById(R.id.signupprooflay1);
        signupprooflay2 = (LinearLayout)view.findViewById(R.id.signupprooflay2);
        signupprooflay3 = (LinearLayout)view.findViewById(R.id.signupprooflay3);
        signupprooflay4 = (LinearLayout)view.findViewById(R.id.signupprooflay4);
        signupprooflay5 = (LinearLayout)view.findViewById(R.id.signupprooflay5);
        videokyc_lay = (LinearLayout)view.findViewById(R.id.videokyc_lay);
        biokyc_lay = (LinearLayout)view.findViewById(R.id.biokyc_lay);
        otpkyc_lay = (LinearLayout)view.findViewById(R.id.otpkyc_lay);


        customeruploadcard = view.findViewById(R.id.customeruploadcard);
        prooflinearlay2 = view.findViewById(R.id.prooflinearlay2);
        prooflinearlay3 = view.findViewById(R.id.prooflinearlay3);
        prooflinearlay4 = view.findViewById(R.id.prooflinearlay4);
        prooflinearlay5 = view.findViewById(R.id.prooflinearlay5);

        reasondetailtxt = (TextView)view.findViewById(R.id.reasondetailtxt);

        text_proof1 = (TextView)view.findViewById(R.id.text_proof1);
        text_proof2 = (TextView)view.findViewById(R.id.text_proof2);
        text_proof3 = (TextView)view.findViewById(R.id.text_proof3);
        text_proof4 = (TextView)view.findViewById(R.id.text_proof4);
        text_proof5 = (TextView)view.findViewById(R.id.text_proof5);

        uploadkyc1 = (TextView)view.findViewById(R.id.signup1);
        uploadkyc2 = (TextView)view.findViewById(R.id.signup2);
        uploadkyc3 = (TextView)view.findViewById(R.id.signup3);
        uploadkyc4 = (TextView)view.findViewById(R.id.signup4);
        uploadkyc5 = (TextView)view.findViewById(R.id.signup5);

        back_img = (ImageView) view.findViewById(R.id.back_img);

        imageView2 = (ImageView) view.findViewById(R.id.upload_imageView2);
        imageView3 = (ImageView) view.findViewById(R.id.upload_imageView3);
        imageView4 = (ImageView) view.findViewById(R.id.upload_imageView4);
        imageView5 = (ImageView) view.findViewById(R.id.upload_imageView5);
        imageView6 = (ImageView) view.findViewById(R.id.upload_imageView6);



        proofSpinner1 = (Spinner)view.findViewById(R.id.upload_proofSpinner1);
        proofSpinner2 = (Spinner)view.findViewById(R.id.upload_proofSpinner2);
        proofSpinner3 = (Spinner)view.findViewById(R.id.upload_proofSpinner3);
        proofSpinner4 = (Spinner)view.findViewById(R.id.upload_proofSpinner4);
        proofSpinner5 = (Spinner)view.findViewById(R.id.upload_proofSpinner5);

        View updatelay = view.findViewById(R.id.updateLay);
        fathername = updatelay.findViewById(R.id.fathername_et);
        mothermaiden = updatelay.findViewById(R.id.mothermaiden_et);
        grossannual = updatelay.findViewById(R.id.grossannual_et);

        isagri = updatelay.findViewById(R.id.isagri_radio);
        genderradio   = updatelay.findViewById(R.id.gender_radio);
        martialstatus = updatelay.findViewById(R.id.martial_radio);

        isagri.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(i == R.id.yes_radiobutton){
                    isAgri = true;
                }
                else{
                    isAgri = false;
                }
            }
        });

        genderradio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton radioButton = radioGroup.findViewById(i);
                gender = radioButton.getText().toString();
            }
        });

        martialstatus.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton radioButton = radioGroup.findViewById(i);
                martialStatus = radioButton.getText().toString();
            }
        });


        submit_update = updatelay.findViewById(R.id.submit_txt);

        back_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });



        biokyc_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                otpdialog("updateentity");
            }
        });

        otpkyc_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                otpdialog("otp");
            }
        });

        uploadkyc1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               uploadKyc();
            }
        });

        uploadkyc2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadKyc();
            }
        });

        uploadkyc3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadKyc();
            }
        });

        uploadkyc4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadKyc();
            }
        });

        uploadkyc5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadKyc();
            }
        });


       /* if (status.equals("FAILED")) {
            if(reason!=null){
                reasoncard.setVisibility(View.VISIBLE);
                reasondetailtxt.setText(reason);
            }
        }*/

        text_proof1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupprooflay1.setVisibility(View.GONE);
                prooflinearlay2.setVisibility(View.VISIBLE);
            }
        });

        text_proof2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupprooflay2.setVisibility(View.GONE);
                prooflinearlay3.setVisibility(View.VISIBLE);
            }
        });

        text_proof3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupprooflay3.setVisibility(View.GONE);
                prooflinearlay4.setVisibility(View.VISIBLE);
            }
        });

        text_proof4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupprooflay4.setVisibility(View.GONE);
                text_proof5.setVisibility(View.GONE);
                prooflinearlay5.setVisibility(View.VISIBLE);
            }
        });

        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proofphoto = "proof1";
            }
        });

        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proofphoto = "proof2";
            }
        });

        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proofphoto = "proof3";
            }
        });
        imageView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proofphoto = "proof4";
            }
        });
        imageView6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                proofphoto = "proof5";
            }
        });

        text_proof5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        submit_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validateFields()) {
                   updateEntity();
                }
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Gson gson = new Gson();
        String json = Constants.getSharedPref(activity,getString(R.string.tokenkey),"usermodelclass");
        userdataInputTo = gson.fromJson(json, UserdataInputTo.class);
        getKycDetails();
    }

    @NonNull
    private RequestBody createPartFromString(String descriptionString) {
        return RequestBody.create(
                okhttp3.MultipartBody.FORM, descriptionString);
    }


    public void getKycDetails(){
        Constants.showLoading(activity);
        final ArrayList<Spinner> spinners = new ArrayList<>();
        spinners.add(proofSpinner1);
        spinners.add(proofSpinner2);
        spinners.add(proofSpinner3);
        spinners.add(proofSpinner4);
        spinners.add(proofSpinner5);

        final ArrayList<ImageView> imageViews = new ArrayList<>();
        imageViews.add(imageView2);
        imageViews.add(imageView3);
        imageViews.add(imageView4);
        imageViews.add(imageView5);
        imageViews.add(imageView6);

        final ArrayList<LinearLayout> signupprooflays = new ArrayList<>();
        signupprooflays.add(signupprooflay1);
        signupprooflays.add(signupprooflay2);
        signupprooflays.add(signupprooflay3);
        signupprooflays.add(signupprooflay4);
        signupprooflays.add(signupprooflay5);

        final ArrayList<CardView> prooflinearlays = new ArrayList<>();
        prooflinearlays.add(customeruploadcard);
        prooflinearlays.add(prooflinearlay2);
        prooflinearlays.add(prooflinearlay3);
        prooflinearlays.add(prooflinearlay4);
        prooflinearlays.add(prooflinearlay5);

        GetKycRequestTo getKycRequestTo = new GetKycRequestTo();
        getKycRequestTo.setBusiness(business);
        getKycRequestTo.setKycRefNo(kitno);
        getKycRequestTo.setBank(userdataInputTo.getBankName());
        String requestcontent = DeviceUtils.getJsonFormat(getKycRequestTo);
        System.out.println("REQbody++"+requestcontent);
        final AllRequestTo allRequestTo = new AllRequestTo();
        try {
            String primary = RequestResponseUtils.getPrimary();
            String reqcontent = RequestResponseUtils.encrypt(requestcontent,primary);
            allRequestTo.setEncryptedKey(RequestResponseUtils.encryptMessage(primary,getActivity()));
            allRequestTo.setEncryptedpayload(reqcontent);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Constants.getEkycServicesAPI(activity,business).getKycDetails(allRequestTo, new Callback<AllRequestTo>() {
            @Override
            public void success(AllRequestTo responseTo, retrofit.client.Response response) {
                Constants.dismissloading();
                try {
                    String reqbody = RequestResponseUtils.decrypt(responseTo.getEncryptedpayload(),Constants.getaesprimaryFromShared(getActivity(),responseTo));
                    GetKycDetailResponseTO getKycDetailResponseTO = (GetKycDetailResponseTO) DeviceUtils.getJsonObj(reqbody,GetKycDetailResponseTO.class);

                    documentslength = getKycDetailResponseTO.getResult().getDocuments().size();

                    /*for (int i=0; i < getKycDetailResponseTO.getResult().getDocuments().size();i++){
                        if(i>0)
                            signupprooflays.get(i-1).setVisibility(View.GONE);

                        prooflinearlays.get(i).setVisibility(View.VISIBLE);

                        if (getKycDetailResponseTO.getResult().getDocuments().get(i).getDocumentType()!=null) {
                            spinners.get(i).setSelection(getIndex(spinners.get(i),getKycDetailResponseTO.getResult().getDocuments().get(i).getDocumentType()));
                        }

                        setImageBitmapBase64(getKycDetailResponseTO.getResult().getDocuments().get(i).getBase64String(),imageViews.get(i));
                    }*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Constants.dismissloading();
                Constants.ShowErrorHandlerMessage(activity,error);
            }
        });
    }
    public void setImageBitmapBase64(String base64,ImageView imageview){
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 8;
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length,options);
        imageview.setImageBitmap(decodedByte);
    }

    private int getIndex(Spinner spinner, String myString){
        for (int i=0;i<spinner.getCount();i++){
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)){
                return i;
            }else if (spinner.getItemAtPosition(i).toString().equals(myString)){
                return i;
            }
        }
        return 0;
    }

    public void mantradevice(Activity activity){
        if(DeviceConnectivity.isPackageInstalled("com.mantra.rdservice",activity.getPackageManager())){
            String pidOption = DeviceConnectivity.getPIDOptions();
            System.out.println("pidoptions++"+pidOption);
        /*String pidOption = "<PidOptions ver=\"2.0\">" +
                            "   <Opts env=\"S\" fCount=\"1\" fType=\"0\" format=\"0\" iCount=\"0\" iType=\"0\" pCount=\"0\" pType=\"0\" pidVer=\"2.0\" posh=\"UNKNOWN\" timeout=\"10000\"/>" +
                            "   <Demo lang=\"05\">" +
                            "   <Pi ms=\"P\" mv=\"100\" name=\"Mahesh\" gender=\"M\"/>" +
                            "   </Demo>" +
                            "</PidOptions>";*/
        /* String pidOption = "<PidOptions ver=\"2.0\">" +
                            "   <Opts env=\"S\" fCount=\"1\" fType=\"0\" format=\"0\" iCount=\"0\" iType=\"0\" otp=\"1234\" wadh=\"Hello\" pCount=\"0\" pType=\"0\" pidVer=\"2.0\" posh=\"UNKNOWN\" timeout=\"10000\"/>" +
                            "   <Demo lang=\"05\">" +
                            "   <Pi ms=\"P\" mv=\"Jigar Shekh\" name=\"Jigar\" lname=\"Shekh\" lmv=\"\" gender=\"M\" dob=\"\" dobt=\"V\" age=\"24\" phone=\"\" email=\"\"/>" +
                            "   <Pa ms=\"E\" co=\"\" house=\"\" street=\"\" lm=\"\" loc=\"\"" +
                            "  vtc=\"\" subdist=\"\" dist=\"\" state=\"\" country=\"\" pc=\"\" po=\"\"/>" +
                            "   <Pfa ms=\"E|P\" mv=\"\" av=\"\" lav=\"\" lmv=\"\"/>" +
                            "   </Demo>" +
                            "</PidOptions>";*/
            Intent intent2 = new Intent(); intent2.setAction("in.gov.uidai.rdservice.fp.CAPTURE");
            intent2.putExtra("PID_OPTIONS", pidOption);
            startActivityForResult(intent2, mantraRequestCode);
        }
        else{
            Constants.showCustomDialog(activity,"Install mantra application");
        }

    }

    private File createVideoFile() throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        imageFileName = photoFileName + timeStamp + "_";
        File storageDir =
                activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".mp4",         /* suffix */
                storageDir      /* directory */
        );

        imageFilePath = image.getAbsolutePath();

        return image;
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if ((resultCode == RESULT_OK) && (requestCode == mantraRequestCode)){
            System.out.println("pidata++"+imageReturnedIntent.getExtras().getString("PID_DATA"));
            xml = imageReturnedIntent.getExtras().getString("PID_DATA");
            if (xml != null) {
                if (xml.contains("Device not ready")) {
                    Constants.showCustomDialog(activity,"Device not connected");
                } else {
                    otpdialog("bio");
                }
            }
        }
    }

    public void consentDialog(final Activity activity, final Dialog otpdialog){
        final Dialog dialog = new Dialog(activity, android.R.style.Theme_Material_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.consentdialog_view);

        CheckBox agreecheck = dialog.findViewById(R.id.iagree);
        CheckBox consentmsg = dialog.findViewById(R.id.consentmsg);
        CheckBox consentmsg1 = dialog.findViewById(R.id.consentmsg1);
        TextView consentsubmit = dialog.findViewById(R.id.consentsubmit);
        TextView multilingual_txt = dialog.findViewById(R.id.multilingual_txt);
        TextView termsandcondition = dialog.findViewById(R.id.termsandcondition_text);

        multilingual_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CopyReadAssets();
            }
        });

        termsandcondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.equitaltrems_str)));
                activity.startActivity(browserIntent);
            }
        });
        consentsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(agreecheck.isChecked() && consentmsg.isChecked() && consentmsg1.isChecked()){
                    dialog.dismiss();
                    otpdialog.show();
                }
                else{
                    Toast.makeText(activity,"Agree all the consent",Toast.LENGTH_LONG).show();
                }
            }
        });

        dialog.show();
    }

    public  void otpdialog(final String from) {
        dialog = new Dialog(activity, android.R.style.Theme_Material_Dialog_Alert);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.otpkyc_view);

        adduser_txt = dialog.findViewById(R.id.adduser_txt);
        et_aadhaar = dialog.findViewById(R.id.et_aadhaar);
        DeviceUtils.setCopyPasteEnableFalse(et_aadhaar);
        et_otp = dialog.findViewById(R.id.et_otpkyc);
        DeviceUtils.setCopyPasteEnableFalse(et_otp);


        submitotpkyc_txt = dialog.findViewById(R.id.submitotpkyc_txt);

        if(from.equals("bio")){
            adduser_txt.setText(activity.getString(R.string.bio_txt));
            submitotpkyc_txt.setText(activity.getString(R.string.submit));
            consentDialog(activity,dialog);
            if (documentType!=null) {
                if (documentType.equals("Form 60")) {
                    callSignaturePad();
                }
            }
        }
        mkloader =  dialog.findViewById(R.id.mkloader);
        clickfirst = true;
        if(from.equals("updateentity")){
            et_aadhaar.setText(customerid);
            et_aadhaar.setEnabled(false);
            dialog.show();
        }

        submitotpkyc_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (from.equals("otp")) {
                    if (clickfirst ){
                        if (et_aadhaar.getText().length() != 0) {
                            generateOtp(et_aadhaar.getText().toString());
                        } else {
                            Toast.makeText(activity,"Enter aadhaar!",Toast.LENGTH_LONG).show();
                        }
                    }
                    else{
                        if (et_otp.getText().length() != 0) {
                          validateOtp(customerid,stan,et_otp.getText().toString());
                        } else {
                            Toast.makeText(activity,"Enter otp!",Toast.LENGTH_LONG).show();
                        }
                    }
                }
                else if(from.equals("updateentity")){
                    if (clickfirst) {
                        generateUpdateOtp();
                    } else {
                        if (et_otp.getText().length() != 0) {
                        validateUpdateOtp(et_otp.getText().toString());
                        } else {
                            Toast.makeText(activity,"Enter otp!",Toast.LENGTH_LONG).show();
                        }
                    }
                }
                else {
                    if (et_aadhaar.getText().length() != 0) {
                        PidDataConfig.pidBlock = xml;
                        PidDataConfig pidDataConfig = new PidDataConfig();
                        //System.out.println("PidBlock++"+new Gson().toJson(pidDataConfig.getPidBlockModels(customerid,et_aadhaar.getText().toString(),Constants.signaturestr)));
                        validateBioKyc(pidDataConfig.getPidBlockModels(customerid,et_aadhaar.getText().toString(),Constants.signaturestr));
                    } else {
                        Toast.makeText(activity,"Enter aadhaar!",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });



    }

    private void callSignaturePad(){
        SignaturePadDialogFragment signaturePadDialogFragment = new SignaturePadDialogFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.addToBackStack("dialog");
        signaturePadDialogFragment.show(ft,"dialog");

    }

    private void CopyReadAssets()
    {
        startActivity(new Intent(activity, PdfViewActivity.class));
    }

    private void generateUpdateOtp(){
        mkloader.setVisibility(View.VISIBLE);
        submitotpkyc_txt.setVisibility(View.GONE);
        GenOtpAndValidateUserRequestTo genOtpAndValidateUserRequestTo = new GenOtpAndValidateUserRequestTo();
        genOtpAndValidateUserRequestTo.setEntityId(customerid);
        String requestcontent = DeviceUtils.getJsonFormat(genOtpAndValidateUserRequestTo);
        final AllRequestTo allRequestTo = new AllRequestTo();
        try {
            String primary = RequestResponseUtils.getPrimary();
            String reqcontent = RequestResponseUtils.encrypt(requestcontent,primary);
            allRequestTo.setEncryptedKey(RequestResponseUtils.encryptMessage(primary,activity));
            allRequestTo.setEncryptedpayload(reqcontent);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Constants.getValidateApi(activity).generateOtp(allRequestTo, new Callback<AllRequestTo>() {
            @Override
            public void success(AllRequestTo responseTo, retrofit.client.Response response) {
                mkloader.setVisibility(View.GONE);
                submitotpkyc_txt.setVisibility(View.VISIBLE);
                try {
                    String reqbody = RequestResponseUtils.decrypt(responseTo.getEncryptedpayload(),Constants.getaesprimaryFromShared(getActivity(),responseTo));
                    GenerateOtpResponseTo generateOtpResponseTo = (GenerateOtpResponseTo) DeviceUtils.getJsonObj(reqbody,GenerateOtpResponseTo.class);

                    if (generateOtpResponseTo.getResult().getSuccess()) {
                        clickfirst = false;
                        et_otp.setVisibility(View.VISIBLE);
                        submitotpkyc_txt.setText(activity.getString(R.string.validateotp));
                    } else {
                        Constants.toastMessage(activity,"Generate otp failed");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Constants.toastMessage(activity,"Generate otp error");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                mkloader.setVisibility(View.GONE);
                submitotpkyc_txt.setVisibility(View.VISIBLE);
                Constants.ShowErrorHandlerMessage(activity,error);
            }
        });
    }

    private void validateUpdateOtp(final String otp){
        mkloader.setVisibility(View.VISIBLE);
        submitotpkyc_txt.setVisibility(View.GONE);
        ValidateOtpRequestTo validateOtpRequestTo = new ValidateOtpRequestTo();
        validateOtpRequestTo.setEntityId(customerid);
        validateOtpRequestTo.setOtp(otp);
        primaryHash = RequestResponseUtils.getPrimary();
        validateOtpRequestTo.setHashPrimary(primaryHash);
        String requestcontent = DeviceUtils.getJsonFormat(validateOtpRequestTo);
        final AllRequestTo allRequestTo = new AllRequestTo();
        try {
            String primary = RequestResponseUtils.getPrimary();
            String reqcontent = RequestResponseUtils.encrypt(requestcontent,primary);
            allRequestTo.setEncryptedKey(RequestResponseUtils.encryptMessage(primary,getActivity()));
            allRequestTo.setEncryptedpayload(reqcontent);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Constants.getValidateApi(activity).validateOtp(allRequestTo, new Callback<AllRequestTo>() {
            @Override
            public void success(AllRequestTo responseTo, retrofit.client.Response response) {
                try {
                    String reqbody = RequestResponseUtils.decrypt(responseTo.getEncryptedpayload(),Constants.getaesprimaryFromShared(getActivity(),responseTo));
                    //System.out.println("reqbody++"+reqbody);
                    ValidateOtpResponseTo validateOtpResponseTo = (ValidateOtpResponseTo) DeviceUtils.getJsonObj(reqbody,ValidateOtpResponseTo.class);

                    if (validateOtpResponseTo.getResult()) {
                        if (validateOtpResponseTo.getHashPrimary().equals(primaryHash)) {
                            mkloader.setVisibility(View.GONE);
                            submitotpkyc_txt.setVisibility(View.VISIBLE);
                            dialog.dismiss();
                            mantradevice(activity);
                        } else {
                            Constants.toastMessage(activity,"Not a valid response!");
                        }
                    }
                } catch (Exception e) {
                    mkloader.setVisibility(View.GONE);
                    submitotpkyc_txt.setVisibility(View.VISIBLE);
                    e.printStackTrace();
                    Constants.toastMessage(activity,"Validate error");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                mkloader.setVisibility(View.GONE);
                submitotpkyc_txt.setVisibility(View.VISIBLE);
                Constants.ShowErrorHandlerMessage(activity,error);
            }
        });
    }

    private Boolean validateFields(){
        if (documentType != null) {
            if(documentType.equals("Form 60")){
                if(grossannual.getText().toString().length() != 0 && Integer.parseInt(grossannual.getText().toString()) > ANNUAL_INCOME){
                    Constants.toastMessage(activity,"Gross annual income was exceeded");
                    return false;
                }
            }
        }

        if(isagri.getCheckedRadioButtonId() == -1){
            Constants.toastMessage(activity,"Agricultural income was not set");
            return false;
        }
        if(genderradio.getCheckedRadioButtonId() == -1){
            Constants.toastMessage(activity,"Gender was not set");
            return false;
        }
        if(martialstatus.getCheckedRadioButtonId() == -1){
            Constants.toastMessage(activity,"Martial Status was not set");
            return false;
        }
        return true;
    }

    private void updateEntity(){
            Constants.showLoading(activity);
            UpdateEntityRequestTo updateEntityRequestTo = new UpdateEntityRequestTo();
            updateEntityRequestTo.setBusinessId(customerid);
            updateEntityRequestTo.setBusinessType(Constants.TENANT);
            updateEntityRequestTo.setEntityId(customerid);
            updateEntityRequestTo.setEntityType("CUSTOMER");
            updateEntityRequestTo.setFatherName(fathername.getText().toString());
            updateEntityRequestTo.setMotherMaidenName(mothermaiden.getText().toString());
            updateEntityRequestTo.setGrossAnnualIncome(grossannual.getText().toString());
            updateEntityRequestTo.setIsAgriculturalIncome(isAgri);
            updateEntityRequestTo.setGender(gender);
            updateEntityRequestTo.setMaritalStatus(martialStatus);
            String requestcontent = DeviceUtils.getJsonFormat(updateEntityRequestTo);
            final AllRequestTo allRequestTo = new AllRequestTo();
            try {
                String primary = RequestResponseUtils.getPrimary();
                String reqcontent = RequestResponseUtils.encrypt(requestcontent,primary);
                allRequestTo.setEncryptedKey(RequestResponseUtils.encryptMessage(primary,getActivity()));
                allRequestTo.setEncryptedpayload(reqcontent);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Constants.getValidateApi(activity).updateUser(allRequestTo, new Callback<AllRequestTo>() {
                @Override
                public void success(AllRequestTo responseTo, retrofit.client.Response response) {
                   Constants.dismissloading();
                    try {
                        String reqbody = RequestResponseUtils.decrypt(responseTo.getEncryptedpayload(),Constants.getaesprimaryFromShared(getActivity(),responseTo));
                        EditKycResponseTO editKycResponseTO = (EditKycResponseTO) DeviceUtils.getJsonObj(reqbody,EditKycResponseTO.class);

                        if (editKycResponseTO.getResult() != null) {
                            Constants.showCustomDialog(activity,editKycResponseTO.getResult());
                        } else {
                            Constants.toastMessage(activity,"Exception occurred");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Constants.dismissloading();
                    Constants.ShowErrorHandlerMessage(activity,error);
                }
            });

    }

    private void generateOtp(final String aadhaarstr){
        mkloader.setVisibility(View.VISIBLE);
        submitotpkyc_txt.setVisibility(View.GONE);
        ValidationRequestTo validationRequestTo = new ValidationRequestTo();
        validationRequestTo.setIdNumber(aadhaarstr);
        validationRequestTo.setIdTypeValidation("U");

        Constants.getValidateApi(activity).generateAadhaarOtpkyc(validationRequestTo, new Callback<OtpkycValidationResponseTo>() {
            @Override
            public void success(OtpkycValidationResponseTo aadharValidationResponseTo, retrofit.client.Response response) {
                mkloader.setVisibility(View.GONE);
                submitotpkyc_txt.setVisibility(View.VISIBLE);

                if (!aadharValidationResponseTo.getBody().getResult().getValidationResponse().getOtpReferenceNo().equals("null")) {
                    stan = aadharValidationResponseTo.getBody().getResult().getValidationResponse().getOtpReferenceNo();
                    clickfirst = false;
                    et_otp.setVisibility(View.VISIBLE);
                    submitotpkyc_txt.setText(activity.getString(R.string.validateotp));
                    callSignaturePad();
                    Constants.toastMessage(activity,aadharValidationResponseTo.getBody().getResult().getValidationResponse().getDescription());
                } else {
                    Constants.showCustomDialog(activity,aadharValidationResponseTo.getBody().getResult().getValidationResponse().getDescription());
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Constants.dismissloading();
                mkloader.setVisibility(View.GONE);
                submitotpkyc_txt.setVisibility(View.VISIBLE);
                Constants.ShowErrorHandlerMessage(activity,error);
            }
        });
    }

    private void validateOtp(final String entityid,final String stan,final String aadhaarotp){
        mkloader.setVisibility(View.VISIBLE);
        submitotpkyc_txt.setVisibility(View.GONE);
        otpVerificationCode = aadhaarotp+"," + Constants.getCurrentdataformat();
        ValidationRequestTo validationRequestTo = new ValidationRequestTo();
        validationRequestTo.setEntityId(entityid);
        validationRequestTo.setStan(stan);
        //System.out.println("PIDOTP+++"+ Constants.removeUTFCharacters(PidDataConfig.otpPid(activity,aadhaarotp)).toString());
        //validationRequestTo.setPidBlock(Constants.removeUTFCharacters(PidDataConfig.otpPid(activity,aadhaarotp)).toString());
        validationRequestTo.setIdTypeValidation("U");
        validationRequestTo.setIdNumber(et_aadhaar.getText().toString());
        validationRequestTo.setConsent("YES");
        validationRequestTo.setOtpVerificationCode(otpVerificationCode);
        validationRequestTo.setSignature(Constants.signaturestr);
        //PidDataConfig.otpPid(activity,aadhaarotp,validationRequestTo);

        Constants.getValidateApi(activity).validateAadhaarOtpkyc(validationRequestTo, new Callback<OtpkycValidationResponseTo>() {
            @Override
            public void success(OtpkycValidationResponseTo otpResponseTo, retrofit.client.Response response) {
                if (otpResponseTo.getBody().getResult() != null) {
                    clickfirst = true;
                    dialog.dismiss();
                    mkloader.setVisibility(View.GONE);
                    submitotpkyc_txt.setVisibility(View.VISIBLE);
                    Constants.dismissloading();
                    Constants.showCustomDialog(activity,otpResponseTo.getBody().getResult().getValidationResponse().getDescription());
                } else {
                    Constants.toastMessage(activity,"Failure");
                }
            }

            @Override
            public void failure(RetrofitError error) {

                mkloader.setVisibility(View.GONE);
                submitotpkyc_txt.setVisibility(View.VISIBLE);
                Constants.ShowErrorHandlerMessage(activity,error);
            }
        });
    }

        public void validateBioKyc(PidblockModels pidblockModels){
        Constants.showLoading(activity);
            String requestcontent = DeviceUtils.getJsonFormat(pidblockModels);
            System.out.println("reqbody++"+requestcontent);
            final AllRequestTo allRequestTo = new AllRequestTo();
            try {
                String primary = RequestResponseUtils.getPrimary();
                String reqcontent = RequestResponseUtils.encrypt(requestcontent,primary);
                allRequestTo.setEncryptedKey(RequestResponseUtils.encryptMessage(primary,getActivity()));
                allRequestTo.setEncryptedpayload(reqcontent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        Constants.getValidateApi(activity).validateBio(allRequestTo, new Callback<AllRequestTo>() {
            @Override
            public void success(AllRequestTo responseTo, retrofit.client.Response response) {
                dialog.dismiss();
                Constants.dismissloading();
                try {
                    String reqbody = RequestResponseUtils.decrypt(responseTo.getEncryptedpayload(),Constants.getaesprimaryFromShared(getActivity(),responseTo));
                    System.out.println("resbody++"+reqbody);
                    ValidateBioResponseTo validateBioResponseTo = (ValidateBioResponseTo) DeviceUtils.getJsonObj(reqbody,ValidateBioResponseTo.class);

                    if (validateBioResponseTo.getResult().getCustLeadId() != null) {
                        Constants.showCustomDialog(activity,"Bio kyc has been submitted successfully!");
                    } else {
                        Constants.toastMessage(activity,"Something went wrong!!");
                    }
                } catch (Exception e) {
                    Constants.toastMessage(activity,"Something went wrong!!");
                    e.printStackTrace();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Constants.dismissloading();
               Constants.ShowErrorHandlerMessage(getActivity(),error);
            }
        });
    }

    public void uploadKyc(){
        Constants.showLoading(activity);
        createorEditKycRequestTo.setCustomerId(customerid);
        createorEditKycRequestTo.setBusiness(business);
        createorEditKycRequestTo.setEkycRefNo(kitno);
        createorEditKycRequestTo.setDocuments(documentslength);

        final String json = new Gson().toJson(createorEditKycRequestTo);
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        //loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                String token = "Basic "+Constants.serviceUrls.SESSIONTOKEN;
                //System.out.println("requestInterceptor upload kycmain"+token);

                Request original = chain.request();

                Request.Builder builder = original.newBuilder();
                builder.header("Content-Type","multipart/form-data");
                builder.header("Authorization",token);

                Request request = builder.method(original.method(), original.body())
                        .build();
                Log.e("header in uploadkyc",request.header("Authorization"));
                Log.e("header in uploadkyc",request.header("Content-Type"));
                return chain.proceed(request);
            }
        });
        httpClient.addInterceptor(loggingInterceptor);

        Retrofit client = new Retrofit.Builder()
                .baseUrl(BuildConfig.KycBaseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        RequestBody jsonstring = createPartFromString(json);
        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("jsonBody",jsonstring);
        //System.out.println("Content uploadkyc++++"+status+jsonstring.contentType()+jsonstring.toString());

        GetUsersServiceAPI getserviceApi = client.create(GetUsersServiceAPI.class);

        if (status.equals("UPLOAD_KYC")) {
            getserviceApi.createKyc(map,parts).enqueue(new retrofit2.Callback<AgentLoginResponseTO>() {
                @Override
                public void onResponse(Call<AgentLoginResponseTO> call, Response<AgentLoginResponseTO> response) {
                    if(response.isSuccessful()){
                        Constants.dismissloading();
                        Constants.showUploadeddialog(activity);
                    }
                    else{
                        Constants.dismissloading();
                        try {
                            Gson gson = new Gson();
                            ResponseTO responseTO = gson.fromJson(response.errorBody().charStream(), ResponseTO.class);
                            Toast.makeText(activity, responseTO.getException().getDetailMessage(), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(activity, "Proxy server error", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<AgentLoginResponseTO> call, Throwable t) {
                    Constants.dismissloading();
                    Log.e("Error in upload",t.getLocalizedMessage());
                    Toast.makeText(activity, "Error in uploadKYC"+t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            getserviceApi.editKyc(map,parts).enqueue(new retrofit2.Callback<EditKycResponseTO>() {
                @Override
                public void onResponse(Call<EditKycResponseTO> call, Response<EditKycResponseTO> response) {
                    if(response.isSuccessful()){
                        Constants.dismissloading();
                        Constants.showUploadeddialog(activity);
                    }
                    else{
                        Constants.dismissloading();
                        try {
                            Gson gson = new Gson();
                            ResponseTO responseTO = gson.fromJson(response.errorBody().charStream(), ResponseTO.class);
                            Toast.makeText(activity, responseTO.getException().getDetailMessage(), Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(activity, "Proxy server error", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<EditKycResponseTO> call, Throwable t) {
                    Constants.dismissloading();
                    Toast.makeText(activity, "Error in uploadKYC"+t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

}
